package at.dgi.games.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class Rect {
	private int x,y;
	private Shape shape;
	private int width, height;
	private int dir;
	
	public Rect(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.dir = 0;
		this.shape = new org.newdawn.slick.geom.Rectangle(x,y,width,height);
	}
	
	public Shape getShape() {
		this.shape.setLocation(x, y);
		return this.shape;
	}
	
	public void render(GameContainer gc, Graphics g) {
		g.draw(getShape());
	}
	
	public void update(GameContainer gc, int arg1) {
		if (this.dir==0) {
			this.x++;
			if(this.x==700 && this.y==0) {
				this.dir=1;
			}
		}
		else if(this.dir==1) {
			this.y++;
			if(this.x==700 && this.y==500) {
				this.dir=2;
			}
		}
		else if(this.dir==2) {
			this.x--;
			if(this.x==0 && this.y==500) {
				this.dir=3;
			}
		}
		else if(this.dir==3) {
			this.y--;
			if(this.x==0 && this.y==0) {
				this.dir=0;
			}
		}
	}
}
