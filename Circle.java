package at.dgi.games.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class Circle {
	private int x,y;
	private Shape shape;
	private int radius;
	
	public Circle(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.shape = new org.newdawn.slick.geom.Circle(x,y,radius);
	}
	
	public Shape getShape() {
		this.shape.setLocation(x, y);
		return this.shape;
	}
	
	public void render(GameContainer gc, Graphics g) {
		g.draw(getShape());
	}
	
	public void update(GameContainer gc, int arg1) {
		if(y>600) {
			y=0;
		}
		else {
			y++;
		}
	}
}
