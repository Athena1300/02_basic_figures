package at.dgi.games.landscape;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Landscape extends BasicGame{

	private Oval oval;
	private Rect rect;
	private Circle circle;
	
	public Landscape() {
		super("Landscape");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		oval.render(gc,g);
		rect.render(gc, g);
		circle.render(gc, g);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		this.oval = new Oval (0,300,100,50);
		this.rect = new Rect(0,0,100,100);
		this.circle = new Circle(350,0,50);
	}

	@Override
	public void update(GameContainer gc, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		oval.update(gc, arg1);
		rect.update(gc, arg1);
		circle.update(gc, arg1);
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
