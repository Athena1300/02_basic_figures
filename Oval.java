package at.dgi.games.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class Oval {
	private int x,y;
	private Shape shape;
	private int width, height;
	private int dir;
	
	public Oval(int x, int y, int width, int height) {
		this.dir = 0;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.shape = new org.newdawn.slick.geom.Ellipse(x,y,width,height);
	}
	
	public Shape getShape() {
		this.shape.setLocation(x, y);
		return this.shape;
	}
	
	public void render(GameContainer gc, Graphics g) {
		g.draw(getShape());
	}
	
	public void update(GameContainer gc, int delta){
		if(this.dir == 0){
			this.x++;
			if(this.x == 600){
				this.dir = 1;
			}
		}
		else if(this.dir == 1){
			this.x--;
			if(this.x == 0){
				this.dir = 0;
			}
		}
	}
}
